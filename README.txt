This module deletes users who meet all following conditions:
1. never logged in
2. registered X days ago (configurable)
3. were not registered by admin, but are "self-registered"
   By "not registered by admin" I understand that the path was
   different then admin/*.
4. registered after the module was loaded

This module is disabled by default. Please read INSTALL.txt
