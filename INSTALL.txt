
1. Load user_maintaince.sql into your database (don't forget to prefix the table if needed)
2. Enable module in admin/modules
3. Set delay in admin/settings/user_maintaince (it's off by default)

Module will work for new users only (because we don't know which already 
existing users were created by admin and which not)

